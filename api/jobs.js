import axios from 'axios';

export const getJobs = async () => {
  const { data } = await axios.get('https://workto.ir/hezardastan/api/job/'); 
  return data;
};

export const getJobDetails = async (id) => {
  const { data } = await axios.get(`https://workto.ir/hezardastan/api/job/${id}`); 
  return data;
};

export const getJobForm = async (id) => {
  const { data } = await axios.get(`https://workto.ir/hezardastan/api/job/${id}/form`); 
  return data;
};

export default {
  getJobs,
  getJobDetails,
};
